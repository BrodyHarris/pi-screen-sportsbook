#!/usr/bin/env python
# Display a runtext with double-buffering.
from samplebase import SampleBase
from rgbmatrix import graphics
import time
import json
import yaml
import os
import sys
from datetime import datetime



class RunText(SampleBase):
    def __init__(self, *args, **kwargs):
        super(RunText, self).__init__(*args, **kwargs)

    def getData(x, cached_data_location):
        print("Refreshing data from " + cached_data_location)
        #make a list for all of the sports json files and iterate through
        all_sports = []
        try:
            for file in os.listdir(cached_data_location):
                if file.endswith(".json"):
                    current_file = os.path.join(cached_data_location, file)
                    opened_file = open(current_file).read()
                    all_sports.append(json.loads(opened_file))
                    print("found " + file + ", displaying now.")
        except:
            return
                
        return all_sports

    def run(self):
        
        #get files and config
        home_directory = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        config_location = os.path.join(home_directory, 'resources', 'config.yaml')
        cached_data_location = os.path.join(home_directory, 'resources', 'cached')

        #get yaml config
        config_file = open(config_location)
        config = yaml.load(config_file, Loader=yaml.FullLoader)
        
        SLEEP_TIME = config['sleep_time']
                
        offscreen_canvas = self.matrix.CreateFrameCanvas()
        
        gamefont = graphics.Font()
        gamefont.LoadFont(os.path.join(home_directory, 'resources', 'fonts', '4x6.bdf'))

        headerfont = graphics.Font()
        headerfont.LoadFont(os.path.join(home_directory, 'resources', 'fonts', '5x7.bdf'))

        headerColor = graphics.Color(255, 180, 0)
        subtitleColor = graphics.Color(255, 255, 255)
        line1color = graphics.Color(10, 200, 80)
        line1subcolor = graphics.Color(10, 220, 130)
        line2color = graphics.Color(10,140, 240)
        line2subcolor = graphics.Color(10,180,250)

        while True:
            #refresh cached data
            all_sports = self.getData(cached_data_location)
            if not all_sports:
                print("No data found. Sleeping for 15 then refresh")
                graphics.DrawText(offscreen_canvas, gamefont, 0, 6, line1color, "no data.")
                offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)
                time.sleep(15)
            color_switch = 1
            #for each sport
            for sport in all_sports:
                #for each page
                for i in range(len(sport)):
                    y_position = 22
                    offscreen_canvas.Clear()
                    for x in range(len(sport[i])):

                        if x == 0:
                            header = sport[i][x][0] + " " + str(i+1) + "/" + str(len(sport))
                            graphics.DrawText(offscreen_canvas, headerfont, 2, 8, headerColor, header)
                            graphics.DrawText(offscreen_canvas, gamefont, 0,16, subtitleColor, sport[i][x][1])
                            print("Displaying " + header)
                            continue

                        for team in sport[i][x]:

                            if color_switch == 1:
                                color = line1subcolor
                            elif color_switch ==2:
                                color = line1color
                            elif color_switch ==3:
                                color = line2subcolor
                            else:
                                color = line2color

                            graphics.DrawText(offscreen_canvas, gamefont, 0, y_position, color, team)
                            y_position +=6
                            
                            if color_switch == 4:
                                color_switch = 1
                            else:
                                color_switch +=1
                    
                    offscreen_canvas = self.matrix.SwapOnVSync(offscreen_canvas)
                    time.sleep(SLEEP_TIME)
                    
            



# Main function
if __name__ == "__main__":
    run_text = RunText()
    if (not run_text.process()):
        run_text.print_help()
