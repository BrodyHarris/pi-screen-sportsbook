import json
import yaml
import os
import sys

class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

home_directory = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
config_location = os.path.join(home_directory, 'resources', 'config.yaml')
current_NHL_odds_location = os.path.join(home_directory, 'resources', 'cached', 'current_NHL_odds.json')
current_NFL_odds_location = os.path.join(home_directory, 'resources', 'cached', 'current_NFL_odds.json')
current_NBA_odds_location = os.path.join(home_directory, 'resources', 'cached', 'current_NBA_odds.json')
current_MLB_odds_location = os.path.join(home_directory, 'resources', 'cached', 'current_MLB_odds.json')

#get yaml config
config_file = open(config_location)
config = yaml.load(config_file, Loader=yaml.FullLoader)

# Grab NBA
current_NBA_odds_file = open(current_NBA_odds_location).read()
NBA_odds = json.loads(current_NBA_odds_file)

# Grab NHL
current_NHL_odds_file = open(current_NHL_odds_location).read()
NHL_odds = json.loads(current_NHL_odds_file)

# Grab NFL
current_NFL_odds_file = open(current_NFL_odds_location).read()
NFL_odds = json.loads(current_NFL_odds_file)

# Grab MLB
current_MLB_odds_file = open(current_MLB_odds_location).read()
MLB_odds = json.loads(current_MLB_odds_file)

#calculate the longest team name
longest = 22;
sport = "ALL"
if len(sys.argv) > 1:
    sport = sys.argv[1].upper()



def print_odds(odds):
    color_switch = True
    for page in odds:
        for game in page:
            color_switch = not color_switch
            if color_switch:
                current_color = color.BLUE
            else:
                current_color = color.CYAN
            for team in game:
                print(current_color + team  + color.END)


if sport == "NFL" or sport == "ALL":
    #print NFL_odds
    print(color.BOLD +color.YELLOW + "\nNATIONAL FOOTBALL LEAGUE BETTING ODDS" + color.END)
    print()
    print_odds(NFL_odds)

if sport == "NHL" or sport == "ALL":
    #print NHL_odds
    print(color.BOLD +color.YELLOW + "\nNATIONAL HOCKEY LEAGUE BETTING ODDS" + color.END)
    print()
    print_odds(NHL_odds)

if sport == "NBA" or sport == "ALL":
    #print NBA_odds
    print(color.BOLD +color.YELLOW + "\nNATIONAL BASKETBALL ASSOCIATION BETTING ODDS" + color.END)
    print()
    print_odds(NBA_odds)

if sport == "MLB" or sport == "ALL":
    #print MLB_odds
    print(color.BOLD +color.YELLOW + "\nMAJOR LEAGUE BASEBALL BETTING ODDS" + color.END)
    print()
    print_odds(MLB_odds)

print()
