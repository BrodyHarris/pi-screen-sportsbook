
import requests
import json
import dateutil.parser as date_parser
from dateutil import tz
from datetime import datetime
import yaml
import os

def load_test_data(test_resp_location):
    # load in the json for TESTING CO
    jsonFile = open(test_resp_location, ).read()
    return json.loads(jsonFile)


def get_api_results(ENDPOINT, API_KEY):

    # get data from web
    odds_response = requests.get(ENDPOINT,
                                 params={
                                     'api_key': API_KEY,
                                     'regions': 'us',
                                     'markets': 'h2h,spreads,totals',
                                     'oddsFormat': 'decimal',
                                     'dateFormat': 'iso',
                                 })
    if odds_response.status_code != 200:
        print(f'Failed to get odds: status_code {odds_response.status_code}, response body {odds_response.text}')
        # Send email out via cron

    else:
        odds_json = odds_response.json()
        print('Number of events:', len(odds_json))

        # Check the usage quota
        print('Remaining requests', odds_response.headers['x-requests-remaining'])
        print('Used requests', odds_response.headers['x-requests-used'])

    return json.loads(odds_response.text)


def format_team_spread(string):
    len_string = len(string)
    if not len_string == 5:
        space_to_add = 5 - len_string
        for i in range(space_to_add):
            string = " " + string

    return string


def format_odds(string):
    len_string = len(string)
    if not len_string == 4:
        space_to_add = 4 - len_string
        for i in range(space_to_add):
            string = string + " "

    return string

def format_team_name(string):
    len_string = len(string)
    if not len_string == 12:
        space_to_add = 12 - len_string
        for i in range(space_to_add):
            string = string + " "

    return string

def format_team_ou(string):
    len_string = len(string)
    if not len_string == 5:
        space_to_add = 5 - len_string
        for i in range(space_to_add):
            string = " " + string

    return string

def format_date_time(string):
    len_string = len(string)
    if not len_string == 8:
        space_to_add = 8-len_string
        for i in range(space_to_add):
            string = " " + string

    return string

def get_new_page(sport, game_page_number):
    page = []
    now = datetime.now()
    current_time = now.strftime("%l:%M%p %-m/%e")
    header = sport + " ODDS " + current_time
    subtitle ="TEAMS        LINES ODDS     TIME"
    header_lines = [header,subtitle]
    page.append(header_lines)
    return page

def format_data_to_list(data, SPORTS_BOOK, BACKUP_SPORTS_BOOK, MAX_GAMES, GAMES_PER_PAGE, sport):
    # keep track of how many games we've processed
    game_number = 0
    game_page_number = 1
    game_on_page_number = 0
    upcoming_games = []
    page =  get_new_page(sport, game_page_number)
    if not data:
        return 

    # GET DATA
    for item in data:

        # Get games
        page =  get_new_page(sport, game_page_number)
    if not data:
        return 

    # GET DATA
    for item in data:

        # Get games
        home_team = item["home_team"]
        home_team_short = home_team.split()[len(home_team.split()) - 1]
        away_team = item["away_team"]
        away_team_short = away_team.split()[len(away_team.split()) - 1]

        # get date for games
        # Convert time zone
        from_zone = tz.tzutc()
        to_zone = tz.tzlocal()
        game_datetime = date_parser.parse(item["commence_time"])
        game_datetime = game_datetime.replace(tzinfo=from_zone)
        game_datetime_MST = game_datetime.astimezone(to_zone)
        game_datetime_MST_dayofw = game_datetime_MST.strftime("%a ")
        game_datetime_MST_daynm = game_datetime_MST.strftime("%-m/%e")

        now = datetime.now()
        current_day = now.strftime("%-m/%e")

        game_datetime_MST_day = ""

        if game_datetime_MST_daynm == current_day:
            game_datetime_MST_day = "Today"
        else:
            game_datetime_MST_day = game_datetime_MST_dayofw + "" + game_datetime_MST_daynm

        game_datetime_MST_time = game_datetime_MST.strftime("%l:%M%p")

        # initialize odds
        home_team_moneyline_odds = "-.--"
        away_team_moneyline_odds = "-.--"
        home_team_spread = "-.-"
        home_team_spread_odds = "-.--"
        away_team_spread = "-.-"
        away_team_spread_odds = "-.--"
        over_under = "--.-"
        over_odds = "-.--"
        under_odds = "-.--"

        found_h2h = False
        found_spread = False
        found_totals = False

        # need to make sure we're getting the odds only from draftkings
        for bookmaker in item["bookmakers"]:
            # loop through bookmakers until found dk
            if bookmaker["key"] == SPORTS_BOOK:
                # loop through those markets and make sure they match the proper outcome we're recording
                for market in bookmaker["markets"]:
                    if market["key"] == "h2h":
                        found_h2h = True;
                        # moneyline odds
                        for outcome in market["outcomes"]:
                            if outcome["name"] == home_team:
                                home_team_moneyline_odds = str(outcome["price"])
                            else:
                                away_team_moneyline_odds = str(outcome["price"])

                    if market["key"] == "spreads":
                        found_spread = True;
                        # Spread odds
                        for outcome in market["outcomes"]:
                            if outcome["name"] == home_team:
                                home_team_spread = str(outcome["point"])
                                home_team_spread_odds = str(outcome["price"])
                            else:
                                away_team_spread = str(outcome["point"])
                                away_team_spread_odds = str(outcome["price"])

                    if market["key"] == "totals":
                        found_totals = True
                        # Over Under
                        for outcome in market["outcomes"]:
                            if outcome["name"] == "Over":
                                over_under = str(outcome["point"])
                                over_odds = str(outcome["price"])
                            elif outcome["name"] == "Under":
                                under_odds = str(outcome["price"])

            ## look for secondary odds
            if not found_h2h:
                if bookmaker["key"] == BACKUP_SPORTS_BOOK:
                    for market in bookmaker["markets"]:
                        if market["key"] == "h2h":
                            found_h2h = True;
                            # moneyline odds
                    for market in bookmaker["markets"]:
                        if market["key"] == "spreads":
                            found_spread = True;
                            # Spread odds
                            for outcome in market["outcomes"]:
                                if outcome["name"] == home_team:
                                    home_team_spread = str(outcome["point"])
                                    home_team_spread_odds = str(outcome["price"])
                                else:
                                    away_team_spread = str(outcome["point"])
                                    away_team_spread_odds = str(outcome["price"])

            if not found_totals:
                if bookmaker["key"] == BACKUP_SPORTS_BOOK:
                    for market in bookmaker["markets"]:
                        if market["key"] == "totals":
                            found_totals = True
                            # Over Under
                            for outcome in market["outcomes"]:
                                if outcome["name"] == "Over":
                                    over_under = str(outcome["point"])
                                    over_odds = str(outcome["price"])
                                elif outcome["name"] == "Under":
                                    under_odds = str(outcome["price"])

        #check if page is full
        if game_on_page_number == GAMES_PER_PAGE:
            #make new page, add old page to list of games
            upcoming_games.append(page)
            game_page_number += 1
            page = get_new_page(sport, game_page_number)
            game_on_page_number = 0;

        # create list for this game
        game = []

        # get abbreviated team names
        home_team_abbreviated = format_team_name(home_team_short)
        away_team_abbreviated = format_team_name(away_team_short)

        # format spreads
        away_team_spread_string = format_team_spread(str(away_team_spread))
        home_team_spread_string = format_team_spread(str(home_team_spread))

        # format OU
        over_string = format_team_ou("" + str(over_under))
        under_string = format_team_ou("U" + str(over_under))

        # format all odds
        away_team_moneyline_odds_string = format_odds(str(away_team_moneyline_odds))
        home_team_moneyline_odds_string = format_odds(str(home_team_moneyline_odds))
        away_team_spread_odds_string = format_odds(str(away_team_spread_odds))
        home_team_spread_odds_string = format_odds(str(home_team_spread_odds))
        over_odds_string = format_odds(str(over_odds))
        under_odds_string = format_odds(str(under_odds))

        # format date and time
        game_day = format_date_time(str(game_datetime_MST_day))
        game_time = format_date_time(str(game_datetime_MST_time)).lower()

        # create list for teams in game
        # these strings will be displayed on the matrix
        away_team_data = away_team_abbreviated +" "+ away_team_moneyline_odds_string +" "+ over_string + " " + game_day
        home_team_data = home_team_abbreviated +" "+ home_team_moneyline_odds_string +" "+ home_team_spread_string +"  "+ game_time

        # add teams to game array
        game.append(away_team_data)
        game.append(home_team_data)

        # add game to games array
        page.append(game)
        #record that we have a new game on the page
        game_on_page_number += 1

        game_number += 1;
        if game_number > MAX_GAMES - 1:
            break

    # if we have a final page, add it
    if game_on_page_number > 0:
        upcoming_games.append(page)
    return upcoming_games


def export_data_to_json(formatted_data, current_odds_location):
    current_odds = json.dumps(formatted_data, indent=4)
    current_odds_file = open(current_odds_location, "w")
    current_odds_file.write(current_odds)


def fetch_basic_odds(sport):

    # set up file locations
    home_directory = os.path.dirname(os.path.dirname(__file__))
    config_location = os.path.join(home_directory, 'resources', 'config.yaml')

    # get yaml config
    config_file = open(config_location)
    config = yaml.load(config_file, Loader=yaml.FullLoader)

    # get general config data
    MAX_GAMES = config["max_games"]
    API_KEY = config["api_key"]
    TESTING = config["testing"]
    SPORTS_BOOK = config["primary_sports_book"]
    GAMES_PER_PAGE= config["games_per_page"]
    # get config data specific to sport
    ENDPOINT = config[sport + "_endpoint"]
    BACKUP_SPORTS_BOOK = config["secondary_sports_book_" + sport]
    test_resp_location = os.path.join(home_directory, 'resources', 'testing', "testing_" + sport + "_odds.json")
    current_odds_location = os.path.join(home_directory, 'resources', 'cached', "current_" + sport + "_odds.json")

    if TESTING:
        # run with fake data
        data = load_test_data(test_resp_location)
    else:
        # run with real data
        data = get_api_results(ENDPOINT, API_KEY)
    # format data
    formatted_data = format_data_to_list(data, SPORTS_BOOK, BACKUP_SPORTS_BOOK, MAX_GAMES, GAMES_PER_PAGE, sport)
    if not formatted_data:
        return
    #export the data
    export_data_to_json(formatted_data, current_odds_location)

def fetch_all_sports():

    # set up file locations
    home_directory = os.path.dirname(os.path.dirname(__file__))
    config_location = os.path.join(home_directory, 'resources', 'config.yaml')

    # get yaml config
    config_file = open(config_location)
    config = yaml.load(config_file, Loader=yaml.FullLoader)

    # get general config data
    SPORTS = config["sports"]

    for sport in SPORTS:
        try:
            fetch_basic_odds(sport)
        except KeyError:
            print("error, deleting odds file")
            #os.remove
